module Geo
  RADIANS_PER_DEGREE = 0.017453293

  module Distance
    # Calculates distance between two locations
    module Haversine
      # Expects point1 and point2 as location objects
      def self.between(point1, point2)
        delta_lat = (point2.lat - point1.lat) * Geo::RADIANS_PER_DEGREE
        delta_lon = (point2.lon - point1.lon) * Geo::RADIANS_PER_DEGREE
        a = Math.sin(delta_lat / 2) * Math.sin(delta_lat / 2) +
            Math.cos(point1.lat * Geo::RADIANS_PER_DEGREE) *
            Math.cos(point2.lat * Geo::RADIANS_PER_DEGREE) *
            Math.sin(delta_lon / 2) * Math.sin(delta_lon / 2)
        2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * 6371
      end
    end
  end
end
