# TODO: more descriptive names
require './geo/distance/haversine'

RSpec.describe Geo::Distance::Haversine do
  describe '#between' do
    it 'should respond to #between method' do
      expect(Geo::Distance::Haversine).to respond_to(:between).with(2).arguments
    end

    it 'should calculate correct distance between two locations' do
      # Use factory girl to reuse locations ?
      location1 = Location.new(lat: 12.9611, lon: 77.6362)
      location2 = Location.new(lat: 12.7409, lon: 77.8253)
      expect(Geo::Distance::Haversine.between(location1, location2)).to eq(31.93402331824408)
    end
  end
end
