# Location class with latitude and longitude
class Location
  attr_accessor :lat, :lon

  def initialize(options)
    options.each { |k, v| instance_variable_set("@#{k}", v) }
  end

  ME = new(lat: 12.9611159, lon: 77.6362214)
end
