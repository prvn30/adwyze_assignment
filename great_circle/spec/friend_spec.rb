require './friend'
require './location'

RSpec.describe Friend do
  let(:friend) { Friend.new(name: 'Dummy name', latitude: '1.2',
                            longitude: '3.4', user_id: '100') }
  # let(:all_friends) { JSON.parse(File.read('data/friends.json'))
  #                         .map { |f| Friend.new(f) }
  #                         .sort_by(&:user_id) }
  describe '#location' do
    it 'should return a location object' do
      expect(friend.location.class).to eq(Location)
    end

    it 'should have correct latitude' do
      expect(friend.location.lat).to eq(1.2)
    end

    it 'should have correct longitude' do
      expect(friend.location.lon).to eq(3.4)
    end

    describe '#all' do
      skip 'read and return data from data/friends.json' do
      end
    end
  end
end
