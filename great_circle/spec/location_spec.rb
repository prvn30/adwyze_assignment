require './friend'
require './location'

RSpec.describe Location do
  it 'should have a valid ME location' do
    expect(Location::ME.lat).to eq(12.9611159)
    expect(Location::ME.lon).to eq(77.6362214)
  end
end
