require 'json'
require './location'
# Friend class
class Friend
  attr_accessor :latitude, :longitude, :user_id, :name

  def initialize(options)
    options.each { |k, v| instance_variable_set("@#{k}", v) }
  end

  def location
    Location.new(lat: latitude.to_f, lon: longitude.to_f)
  end

  def distance_from_me
    Geo::Distance::Haversine.between(location, Location::ME)
  end

  # Class method ( method to mock functionality similar to activerecord )
  # Usually, this will be a database call from active record
  def self.all
    JSON.parse(File.read('data/friends.json')).map { |f| Friend.new(f) }
        .sort_by(&:user_id)
  rescue JSON::ParserError, Errno::ENOENT => e
    puts e.message
  end
end
