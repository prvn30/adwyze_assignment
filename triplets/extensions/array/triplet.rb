module Extensions
  module Array
    # Module to find triplets in an array
    module Triplet
      # Returns an array of arrays whose sum of elements equals target_sum
      def triplet_eq_to(target_sum)
        validate
        sorted_array, combinations = sort, []
        (0..sorted_array.length - 3).each do |i|
          front = sorted_array[i]
          start_index = i + 1
          back_index = sorted_array.length - 1

          while start_index < back_index
            start = sorted_array[start_index]
            back = sorted_array[back_index]

            if (front + start + back) > target_sum
              back_index -= 1
            else
              start_index += 1
            end

            combinations.push([start, front, back]) if front + start + back == target_sum
          end
        end
        combinations.uniq
      end

      def triplet_less_than; end

      def triplet_greater_than; end

      private

      def validate
        length_less_than_three
        only_integers_allowed
      end

      def only_integers_allowed
        msg = 'only integers are allowed'
        raise ArgumentError, msg, caller if any? { |e| e.class != Fixnum }
      end

      def length_less_than_three
        msg = 'minimum length of input array is 3'
        raise ArgumentError, msg, caller if length < 3
      end
    end
  end
end
