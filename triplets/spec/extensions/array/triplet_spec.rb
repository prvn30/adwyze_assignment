# TODO: more descriptive names
# TODO: write specs for exceptions in triplet module
require './extensions/array/triplet'

RSpec.describe Array do
  Array.include Extensions::Array::Triplet
  let(:array) { [] }
  describe 'triplet_eq_to' do
    it 'should respond to #triplet_eq_to method' do
      expect(array).to respond_to(:triplet_eq_to).with(1).argument
    end

    it 'should find triplets that sum upto given target' do
      array = [1, 2, 3, 4]
      expect(array.triplet_eq_to(9).map { |e| e.inject(:+) }.uniq).to eq([9])
    end

    it 'should return an empty array if the triplet is not found' do
      array = [1, 2, 4, 5]
      expect(array.triplet_eq_to(3)).to be_empty
    end

    it 'should find triplets that sum upto given target' do
      array = [1, 2, 3, 4]
      expect(array.triplet_eq_to(6).map { |e| e.inject(:+) }.uniq).to eq([6])
    end

    it 'should find triplets that sum upto given target' do
      array = [1, 1, 5, 2, 3, 2]
      expect(array.triplet_eq_to(7).map { |e| e.inject(:+) }.uniq).to eq([7])
    end

    it 'should find multiple triplets that sum upto given target' do
      # [5, 3, 2], [2, 3, 2]]
      array = [1, 1, 5, 2, 3, 2]
      expect(array.triplet_eq_to(7).length).to eq(2)
    end

    it 'should find multiple triplets that sum upto given target' do
      array = [-11, -101, 55, 26, 33, 20]
      expect(array.triplet_eq_to(-92).map { |e| e.inject(:+) }.uniq).to eq([-92])
    end

    it 'should find multiple triplets that sum upto given target' do
      array = [0, 0, 0, 0, 0]
      expect(array.triplet_eq_to(0).map { |e| e.inject(:+) }.uniq).to eq([0])
    end
  end
end
