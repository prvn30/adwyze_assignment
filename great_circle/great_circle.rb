# TODO: Bring friend, location under same module and require
# it so that we don't have to use require for every new class
require './geo/distance/haversine'
require './friend'
require './location'

Friend.all.each do |friend|
  distance = friend.distance_from_me
  if distance < 100
    puts "#{friend.name} with user_id #{friend.user_id} is #{distance} far"\
         ' from office and can be invited'
  end
end

# puts Friend.all.map{|f| f.attributes.to_json}
